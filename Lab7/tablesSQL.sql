drop table Sale;
drop table Book;

create table Book(
	id		 		serial primary key,
	knowledge 		varchar(40),
	book_name 		varchar(50),
	author_name		varchar(50),
	creation_year	int
);

create table Sale(
	id 				serial primary key,
	book_id 		integer references Book (book_id),
	number			integer,
	sale_date 		date
);

insert into book(knowledge, book_name, author_name, creation_year) values 
	( 'Fiction', 'Harry Potter', 'J.K. Rowling', 1997),
	( 'Detective', 'Sherloc Holmes', 'A.C. Doyle', 1887),
	( 'Horror', 'Dracula', 'Bram Stoker', 1897);
	
insert into sale(book_id, number_of_books, sale_date) values 
	( 1, 20, '2010-01-20'),
	( 1, 22, '2012-07-20'),
	( 1, 1, '2020-03-22'),
	( 2, 3, '2019-06-09'),
	( 2, 20, '2018-02-22');