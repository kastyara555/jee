package gsu.by.lab7.repositories;

import gsu.by.lab7.beans.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface BookRepo extends JpaRepository<Book,Integer> {
    Book getOne(Integer id);
    @Query("select b from Book b where name=:sName")
    Book getOne(@Param("sName") String sName);

}
