package gsu.by.lab7.repositories;

import gsu.by.lab7.beans.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface SaleRepo extends JpaRepository<Sale,Integer> {
    Sale getOne(Integer id);
    List<Sale> getSalesByBook(int book);
    List<Sale> getSalesByDateBefore(Date date);
    List<Sale> getSalesByBookAndDateBefore(int book, Date date);
}
