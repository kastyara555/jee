package gsu.by.lab7.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import gsu.by.lab7.beans.Book;
import gsu.by.lab7.repositories.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {
    @Autowired
    private BookRepo bookRepo;

    @GetMapping("/book")
    public List<Book> getBooks(){
        List<Book> b = bookRepo.findAll();
        return b;
    }

    @GetMapping("/book/name")
    public Book getBookByName(@RequestParam(name = "bookName") String name){
        Book b = bookRepo.getOne(name);
        return b;
    }

    @GetMapping("/book/{id}")
    public Book getBookById(@PathVariable int id){
        Book b = bookRepo.getOne(id);
        return b;
    }

    @PostMapping("/book")
    Book newBook(@RequestParam(name = "name") String name
            ,@RequestParam(name = "author") String author
            ,@RequestParam(name = "knowledge") String knowledge
            ,@RequestParam(name = "year") String year) throws JsonProcessingException {
        Book book = new Book(knowledge,name,author,Integer.parseInt(year));
        return bookRepo.save(book);
    }

    @PutMapping("/book")
    Book replaceBook(
            @RequestBody Book b) {

        return bookRepo.findById(b.getId())
                .map(oldBook -> {
                    oldBook.setKnowledge(b.getKnowledge());
                    oldBook.setCreationYear(b.getCreationYear());
                    oldBook.setName(b.getName());
                    oldBook.setAuthor(b.getAuthor());
                    return bookRepo.save(oldBook);
                })
                .orElseGet(() -> {
                    return bookRepo.save(new Book(b.getKnowledge(),b.getName(),b.getAuthor(),b.getCreationYear()));
                });
    }

    @DeleteMapping("/book")
    void deleteBook(@RequestParam(name = "id") String id) {
        bookRepo.deleteById(Integer.parseInt(id));
    }
}
