package gsu.by.lab7.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import gsu.by.lab7.beans.Book;
import gsu.by.lab7.beans.Sale;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpHeaders.USER_AGENT;

@Controller
@RequestMapping("/home")
public class HomeController {
    private String bookUrl = "http://localhost:8080/book";
    private final RestTemplate rt = new RestTemplate();
    @GetMapping
    public String home(Map<String,Object> model){
        ResponseEntity<Book[]> r;
        String url = bookUrl;
        r = rt.getForEntity( url, Book[].class);
        List<Book> b = Arrays.asList(r.getBody());
        model.put("books",b);

        ResponseEntity<Sale[]> s;
        String url1 = "http://localhost:8080/sale";
        s = rt.getForEntity( url1, Sale[].class);
        List<Sale> sa = Arrays.asList(s.getBody());
        model.put("sales",sa);

        return "home";
    }
    @GetMapping("/get")
    public String getOne(String id, String bookName, String date, Map<String,Object> model){
        if(id.equals("") && !bookName.equals("")){
            ResponseEntity<Book> b;
            b = rt.getForEntity("http://localhost:8080/book/name?bookName=" + bookName, Book.class);
            id = String.valueOf(((Book) b.getBody()).getId());
        }
        ResponseEntity<Sale[]> r;
        r = rt.getForEntity("http://localhost:8080/sale/criteria?book="+id+"&date="+date, Sale[].class);
        List<Sale> s = Arrays.asList(r.getBody());
        String message = "";
        for (Sale q :
                s) {
            message += q + "\n";
        }
        model.put("message",message);
        return "oneRes";
    }
    @PostMapping("/change")
    public String change(String bId, String bookName, String author, String knowledge, String year, Map<String,Object> model){
        if(bId.equals("")){
            try {
                URL obj = new URL("http://localhost:8080/book");
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("User-Agent", USER_AGENT);
                con.setDoOutput(true);
                OutputStream os = con.getOutputStream();
                String POST_PARAMS = "id=" + bId + "&name=" + bookName + "&author=" + author + "&knowledge=" + knowledge + "&year=" + year;
                os.write(POST_PARAMS.getBytes());
                os.flush();
                os.close();
                int responseCode = con.getResponseCode();
                System.out.println("POST Response Code :: " + responseCode);

                if (responseCode == HttpURLConnection.HTTP_OK) { //success
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                }

            }catch (Exception e){

            }
        } else {
            if (!bookName.equals("") || !author.equals("") || !knowledge.equals("") || !year.equals("") ){
                try {
                    URL obj = new URL("http://localhost:8080/book");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("PUT");
                    con.setDoOutput(true);
                    con.setRequestProperty("Content-type", "application/json");
                    OutputStream os = con.getOutputStream();
                    Book book = new Book(knowledge,bookName,author,Integer.parseInt(year));
                    book.setId(Integer.parseInt(bId));
                    new ObjectMapper().writeValue(os,book);
                    os.flush();
                    os.close();
                    int responseCode = con.getResponseCode();
                    System.out.println("POST Response Code :: " + responseCode);

                    if (responseCode == HttpURLConnection.HTTP_OK) { //success
                        BufferedReader in = new BufferedReader(new InputStreamReader(
                                con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                    }

                }catch (Exception e){

                }
            } else {
                try {
                    URL obj = new URL("http://localhost:8080/book");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("DELETE");
                    con.setRequestProperty("User-Agent", USER_AGENT);
                    con.setDoOutput(true);
                    OutputStream os = con.getOutputStream();
                    String POST_PARAMS = "id=" + bId;
                    os.write(POST_PARAMS.getBytes());
                    os.flush();
                    os.close();
                    int responseCode = con.getResponseCode();
                    System.out.println("POST Response Code :: " + responseCode);

                    if (responseCode == HttpURLConnection.HTTP_OK) { //success
                        BufferedReader in = new BufferedReader(new InputStreamReader(
                                con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                    }

                }catch (Exception e){

                }
            }

        }
        return "redirect:/home";
    }

    @PostMapping("/sale/change")
    public String change(String sId, String book, String number, String date, Map<String,Object> model){
        if(sId.equals("")){
            try {
                URL obj = new URL("http://localhost:8080/sale");
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("User-Agent", USER_AGENT);
                con.setDoOutput(true);
                OutputStream os = con.getOutputStream();
                String POST_PARAMS = "id=" + sId + "&book=" + book + "&number=" + number + "&date=" + date;
                os.write(POST_PARAMS.getBytes());
                os.flush();
                os.close();
                int responseCode = con.getResponseCode();
                System.out.println("POST Response Code :: " + responseCode);

                if (responseCode == HttpURLConnection.HTTP_OK) { //success
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                }

            }catch (Exception e){

            }
        } else {
            if (!book.equals("") || !number.equals("") || !date.equals("")){
                try {
                    System.out.println("qweqweqwe");
                    URL obj = new URL("http://localhost:8080/sale");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("PUT");
                    con.setDoOutput(true);
                    con.setRequestProperty("Content-type", "application/json");
                    OutputStream os = con.getOutputStream();
                    Sale sale = new Sale(Integer.parseInt(book),Integer.parseInt(number),date);
                    sale.setId(Integer.parseInt(sId));
                    new ObjectMapper().writeValue(os,sale);
//                    os.write(new ObjectMapper().writeValueAsString(book).getBytes());
                    os.flush();
                    os.close();
                    int responseCode = con.getResponseCode();
                    System.out.println("POST Response Code :: " + responseCode);

                    if (responseCode == HttpURLConnection.HTTP_OK) { //success
                        BufferedReader in = new BufferedReader(new InputStreamReader(
                                con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                    }

                }catch (Exception e){

                }
            } else {
                try {
                    URL obj = new URL("http://localhost:8080/sale");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("DELETE");
                    con.setRequestProperty("User-Agent", USER_AGENT);
                    con.setDoOutput(true);
                    OutputStream os = con.getOutputStream();
                    String POST_PARAMS = "id=" + sId;
                    os.write(POST_PARAMS.getBytes());
                    os.flush();
                    os.close();
                    int responseCode = con.getResponseCode();
                    System.out.println("POST Response Code :: " + responseCode);

                    if (responseCode == HttpURLConnection.HTTP_OK) { //success
                        BufferedReader in = new BufferedReader(new InputStreamReader(
                                con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();
                    }

                }catch (Exception e){

                }
            }

        }
        return "redirect:/home";
    }
}
