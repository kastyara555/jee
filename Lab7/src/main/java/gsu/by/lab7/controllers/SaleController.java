package gsu.by.lab7.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import gsu.by.lab7.beans.Book;
import gsu.by.lab7.beans.Sale;
import gsu.by.lab7.repositories.BookRepo;
import gsu.by.lab7.repositories.SaleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;

@RestController
public class SaleController {
    @Autowired
    private SaleRepo saleRepo;
    @Autowired
    private BookRepo bookRepo;

    @GetMapping("/sale/criteria")
    public List<Sale> criteriaGet(@RequestParam(name = "book") String book
            ,@RequestParam(name = "date") String date){
        if(book.equals("") && !date.equals(""))
            return saleRepo.getSalesByDateBefore(Date.valueOf(date));
        else if(!book.equals("") && date.equals(""))
            return saleRepo.getSalesByBook(Integer.parseInt(book));
        else
            return saleRepo.getSalesByBookAndDateBefore(Integer.parseInt(book), Date.valueOf(date));
    }

    @GetMapping("/sale")
    public List<Sale> getSales(){
        List<Sale> b = saleRepo.findAll();
        return b;
    }

    @GetMapping("/sale/{id}")
    public Sale getSaleById(@PathVariable int id){
        Sale b = saleRepo.getOne(id);
        return b;
    }

    @PostMapping("/sale")
    public Sale newSale(@RequestParam(name = "book") String book
            ,@RequestParam(name = "number") String number
            ,@RequestParam(name = "date") String date) throws JsonProcessingException {
        Sale sale = new Sale(Integer.parseInt(book),Integer.parseInt(number),date);
        return saleRepo.save(sale);
    }

    @PutMapping("/sale")
    Sale replaceSale(
            @RequestBody Sale b) {

        return saleRepo.findById(b.getId())
                .map(oldBook -> {
                    oldBook.setBook(b.getBook());
                    oldBook.setNumber(b.getNumber());
                    oldBook.setDate(b.getDate());
                    return saleRepo.save(oldBook);
                })
                .orElseGet(() -> {
                    return saleRepo.save(new Sale(b.getBook(),b.getNumber(),b.getDate()));
                });
    }

    @DeleteMapping("/sale")
    void deleteSale(@RequestParam(name = "id") String id) {
        saleRepo.deleteById(Integer.parseInt(id));
    }
}