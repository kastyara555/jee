package gsu.by.lab7;

import com.fasterxml.jackson.databind.ObjectMapper;
import gsu.by.lab7.beans.Book;
import junit.framework.TestCase;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static org.springframework.http.HttpHeaders.USER_AGENT;

public class Tester extends TestCase {
    public Tester() throws MalformedURLException {
    }

    public void testToHexString() throws IOException {
        URL obj = new URL("http://localhost:8080/book");
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setDoOutput(true);
        OutputStream os = con.getOutputStream();
        String POST_PARAMS = "id=" + 1 + "&name=" + "bookName" + "&author=" + "author" + "&knowledge=" + "knowledge" + "&year=" + 1234;
        os.write(POST_PARAMS.getBytes());
        os.flush();
        os.close();
        int responseCode = con.getResponseCode();
        System.out.println("POST Response Code :: " + responseCode);

        assertEquals(responseCode, HttpURLConnection.HTTP_OK);

        obj = new URL("http://localhost:8080/book");
        con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("PUT");
        con.setDoOutput(true);
        con.setRequestProperty("Content-type", "application/json");
        os = con.getOutputStream();
        Book book = new Book("knowledge2", "bookName2", "author2", 4444);
        book.setId(1);
        new ObjectMapper().writeValue(os, book);
        os.flush();
        os.close();
        responseCode = con.getResponseCode();

        assertEquals(responseCode, HttpURLConnection.HTTP_OK);

        obj = new URL("http://localhost:8080/book");
        con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("DELETE");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setDoOutput(true);
        os = con.getOutputStream();
        POST_PARAMS = "id=" + 1;
        os.write(POST_PARAMS.getBytes());
        os.flush();
        os.close();
        responseCode = con.getResponseCode();

        assertEquals(responseCode, HttpURLConnection.HTTP_OK);
    }
}