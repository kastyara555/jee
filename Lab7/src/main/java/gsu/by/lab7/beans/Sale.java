package gsu.by.lab7.beans;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name="sale")
public class Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "book_id")
    private int book;
    @Column(name = "number")
    private int number;
    @Column(name = "date")
    private Date date;

    public Sale(){
    }

    public Sale(int book, int number, Date date) {
        this.book = book;
        this.number = number;
        this.date = date;
    }

    public Sale(int book, int number, String date) {
        this.book = book;
        this.number = number;
        this.date = Date.valueOf(date);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBook() {
        return book;
    }

    public void setBook(int book) {
        this.book = book;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Продажа : " +
                "номер чека - '" + id + '\'' +
                ", код книги - '" + book + '\'' +
                ", количество - '" + number + '\'' +
                ", дата продажи - '" + date + '\'' +
                '.';
    }
}
