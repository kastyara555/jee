package gsu.by.lab7.beans;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "book_name",unique = true)
    private String name;
    @Column(name = "knowledge")
    private String knowledge;
    @Column(name = "author_name")
    private String author;
    @Column(name = "creation_year")
    private int creationYear;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public Book(){
    }

//    public Book(Integer id){
//        this.setId(id);
//    }

    public Book(String knowledge, String name, String author, int creationYear) {
        this.knowledge = knowledge;
        this.name = name;
        this.author = author;
        this.creationYear = creationYear;
    }

//    public Book(String knowledge, String name, String author, int creationYear) {
//        this.setId(id);
//        this.knowledge = knowledge;
//        this.name = name;
//        this.author = author;
//        this.creationYear = creationYear;
//    }

    public String getKnowledge() {
        return knowledge;
    }

    public void setKnowledge(String knowledge) {
        this.knowledge = knowledge;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getCreationYear() {
        return creationYear;
    }

    public void setCreationYear(int creationYear) {
        this.creationYear = creationYear;
    }

    @Override
    public String toString() {
        return "Книга : " +
                "код - '" + id + '\'' +
                ",название - '" + name + '\'' +
                ", область знаний - '" + knowledge + '\'' +
                ", автор - '" + author + '\'' +
                ", год написания - " + creationYear +
                '.';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return creationYear == book.creationYear &&
                Objects.equals(name, book.name) &&
                Objects.equals(knowledge, book.knowledge) &&
                Objects.equals(author, book.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, knowledge, author, creationYear);
    }
}
