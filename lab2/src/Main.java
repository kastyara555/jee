import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.toMap;

public class Main {

    public static void main(String[] args) throws IOException {


        List<Grades> grades = getGrades();
        List<Student> students = getStudents();

        System.out.println("Старше 18:");
        students.stream().filter(s -> s.getAge() < 18).sorted((a, b) -> a.getFio().compareTo(b.getFio())).forEach(System.out::println);
        System.out.println("Группы без повторов:");
        students.stream().map(Student::getGroup).distinct().forEach(System.out::println);

        System.out.println("Средний возраст студентов факультета:");
        students.stream().collect(Collectors.groupingBy(Student::getFac,
                Collectors.averagingDouble(Student::getAge)))
                .entrySet().stream()
                .sorted(comparingByValue(Comparator.reverseOrder()))
                .collect(
                        toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
                                LinkedHashMap::new)).forEach((k, v) -> System.out.println(k + " - " + v));

        System.out.println("Вывод списка студентов заданной группы у которых сдан 3 экзамен (>4)");
        String groupName = "p-1";
        List<String> studs = students.stream().filter(s -> s.getGroup().equals(groupName))
                .map(Student::getFio).collect(Collectors.toList());
        grades.stream().filter(g -> studs.contains(g.getFio())).filter(g -> g.getGrage3() > 4).forEach(System.out::println);


        System.out.println("Определение факультета с максимальной средней оценкой по первому экзамену");
        Map<String, List<Student>> studByFac = students.stream().collect(Collectors.groupingBy(s -> s.getFac()));

        Map<String, Double> res = studByFac.entrySet().stream().collect(Collectors.toMap(e -> e.getKey(), e ->
                grades.stream().filter(g ->
                        e.getValue().stream().map(Student::getFio).collect(Collectors.toList()).contains(g.getFio()))
                        .mapToInt(Grades::getGrage1).average().getAsDouble()));

        Map.Entry<String, Double> maxEntry = res.entrySet().stream()
                .max(Map.Entry.comparingByValue()).get();
        System.out.println(maxEntry.getKey() + " " + maxEntry.getValue());


        System.out.println("Определение количества студентов в каждой группе");
        students.stream().collect(Collectors.groupingBy(s -> s.getGroup()))
                .forEach((k, v) -> System.out.println(k + " " + v.size()));

        System.out.println("Определение минимального возраста для каждого факультета");
        students.stream().collect(Collectors.groupingBy(s -> s.getFac()))
                .forEach((k, v) -> System.out.println(k + " " + v.stream().mapToInt(Student::getAge).min().getAsInt()));
    }

    public static List<Student> getStudents() throws IOException {
        try (BufferedReader csvReader = new BufferedReader(new FileReader("students.csv"))) {
            List<Student> res = new ArrayList<>();
            String row;
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(";");
                Student s = new Student(data[0], data[1], data[2], Integer.parseInt(data[3]));
                res.add(s);
            }
            return res;
        }
    }

    public static List<Grades> getGrades() throws IOException {
        try (BufferedReader csvReader = new BufferedReader(new FileReader("grades.csv"))) {
            List<Grades> res = new ArrayList<>();
            String row;
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(";");
                Grades s = new Grades(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]), Integer.parseInt(data[3]));
                res.add(s);
            }
            return res;
        }
    }
}
