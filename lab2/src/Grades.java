public class Grades {
    private String fio;
    private int grage1;
    private int grage2;
    private int grage3;

    public Grades(String fio, int grage1, int grage2, int grage3) {
        this.fio = fio;
        this.grage1 = grage1;
        this.grage2 = grage2;
        this.grage3 = grage3;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public int getGrage1() {
        return grage1;
    }

    public void setGrage1(int grage1) {
        this.grage1 = grage1;
    }

    public int getGrage2() {
        return grage2;
    }

    public void setGrage2(int grage2) {
        this.grage2 = grage2;
    }

    public int getGrage3() {
        return grage3;
    }

    public void setGrage3(int grage3) {
        this.grage3 = grage3;
    }

    @Override
    public String toString() {
        return "Grades{" +
                "fio='" + fio + '\'' +
                ", grage1=" + grage1 +
                ", grage2=" + grage2 +
                ", grage3=" + grage3 +
                '}';
    }
}
