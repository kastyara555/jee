public class Student {
    private String fio;
    private String group;
    private String fac;
    private int age;

    public Student(String fio, String group, String fac, int age) {
        this.fio = fio;
        this.group = group;
        this.fac = fac;
        this.age = age;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getFac() {
        return fac;
    }

    public void setFac(String fac) {
        this.fac = fac;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "fio='" + fio + '\'' +
                ", group='" + group + '\'' +
                ", fac='" + fac + '\'' +
                ", age=" + age +
                '}';
    }
}
